import Common from './Pages/_Common';

class Main {
    constructor() {
        this.init();
    }

    init() {
        new Common();
    }
}

new Main();