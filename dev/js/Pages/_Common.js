
export default class Common {
    constructor() {
        this.init();
    }

    DOMReady() {
        $(document).ready(() => {
            this.carousel();
            this.voltarTop();
            this.validaForm();
        });
    } 

    onLoad() {
        $(window).on('load', () => {
            console.log("Onload");
            
        });
    } 
    
    validaForm() {
        $('form').find('button').click(function(event){
            event.preventDefault();

            let nome = $('form').find('#nome');
            let email = $('form').find('#email');
            let mensagem = $('form').find('#mensagem');
            
            function regexMail(email) {
                var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
                return regex.test(email);
              }

            let validaNome = nome.val() == "";
            let validaEmail = email.val() == "" || !regexMail(email.val());

            if(validaNome){
                nome.addClass("erro");
                return;
             }

             if(validaEmail){
                nome.removeClass("erro");
                email.addClass("erro");
                return;
             }          

            let data = {
                'name': nome.val(),
                'email': email.val(),
                'message': mensagem.val()
            }

            if(!validaNome && !validaEmail){
                $('.form').find('input').removeClass("erro");
                alert("Dados enviados com sucesso!")
            }

        });
    }
    
    voltarTop() {
        $(".voltarTop").on("click", function (event) {
            event.preventDefault()
            $('html, body').animate({scrollTop:0}, 'slow'); 
        });
    }

    carousel (){
        $('.s-prateleira__list').owlCarousel({
            nav: true,
            dots: false,
            autoplay: true,
            navText : ["",""],
            rewindNav : true,
            responsiveClass: true,
            items: 4,
            loop: true,
            margin: 30,
            nav: true,
            smartSpeed: 900,
            navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 2,
                },
                765: {
                    items: 2,
                },
                1000: {
                    items: 3
                }
            }
        })
    }

    init() {
        this.DOMReady();
        this.onLoad();
    }
}