<!DOCTYPE html>
<html lang="pt">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SuperGiantGames</title>
<!-- Styles -->
<link href="assets/css/cardgame.css" rel="stylesheet">
<body>
<header class="s-header">
    <div class="container">
        <h1 class="s-header__logo">
            <a href="#" title="SuperGiantGames">
                <img src="assets/img/logo.png" alt="SuperGiantGames" />
                <span>SuperGiantGames</span>
            </a>
        </h1>
    </div>
</header>       

<main class="s-man">
    <section class="s-vitrine">
    </section>

    <section class="s-prateleira">
        <div class="container">
            <ul class="s-prateleira__list owl-carousel">
                <li class="s-prateleira__list--item">
                    <a href="#">
                        <img src="assets/img/personagem-item-01.png" alt="">
                        <p>A Camerata foi apenas os dois no início, e suas fileiras nunca foram destinadas a exceder um número a ser contado em uma mão.</p>
                    </a>
                </li>
                <li class="s-prateleira__list--item">
                    <a href="#">
                        <img src="assets/img/personagem-item-02.png" alt="">
                        <p>Red, uma jovem cantora, entrou em posse do Transistor. Sendo a poderosa espada falante. O grupo Possessores quer tanto ela quanto o Transistor e está perseguindo implacavelmente a sua procura.</p>
                    </a>
                </li>
                <li class="s-prateleira__list--item">
                    <a href="#">
                        <img src="assets/img/personagem-item-03.png" alt="">
                        <p>Sybil é descrita pelo Transistor como sendo os "olhos e ouvidos" da Camerata.</p>
                    </a>
                </li>
            </ul>
        </div>
    </section>
</main>
  
<footer class="s-footer">
    <div class="s-footer__form">
        <div class="s-footer__form--content">
            <h3>FORMULÁRIO</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>    
            <form action="">
                <ul>
                    <li>
                        <input type="text" id="nome" placeholder="Nome" />
                        <input type="email" id="email" placeholder="E-mail" />
                    </li>
                    <li>
                        <textarea name="mensagem" id="mensagem" placeholder="Mensagem" cols="30" rows="10"></textarea>
                    </li>
                    <li>
                        <button type="submit">Enviar</button>
                    </li>
                </ul>
            </form>
        </div>
    </div>
</footer>

<a href="javascript:;" class="voltarTop"><i class="fas fa-angle-up"></i></a>
</body>
<!-- SCRIPTS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- OWL CAROUSSEL-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="assets/js/cardgame.js"></script>
</html>
